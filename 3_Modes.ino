void animations(int mode) {
  switch (mode) {
    case -1: // All Black
      FOR_ALL_LEDS {
        leds[i] = CRGB::Black;
      }
      break;
    case 0: // Basic spinning gradient
      {
        static objPlane plane = {.v = {0, 0, 1000}};
        static int dirVector[3];
        if (newMode) {
          randomUnitVector(dirVector);
          newMode = false;
        }
        
        fadeLedsBy(255);

        // Plane parameters
        planeDefaults();
        planeWidth = 40000;
        planeFade = 0;
        paletteIndex = millis()/20;
        paletteScale = 200;
        planeBri = 255;
        
        renderPlane(&plane);
        walkPlane(&plane, dirVector, 100, 50, 1000);
      }
      break;
      
    case 1: // Old simple gradient animation in random order
      if (newMode) {newMode = false;}
      colorwaves( leds, NUM_LEDS, gCurrentPalette);
      break;
      
    case 2: // Random bouncing waves (5)
      {
        #define NUM_WAVES 5
        static objPlane planes[NUM_WAVES];
        static int dirVectors[NUM_WAVES][3];
        if (newMode) {
          for (int i = 0; i < NUM_WAVES; i++) {
            randomPlane(&planes[i], random(20000, 25000));
            randomUnitVector(dirVectors[i]);
          }
          newMode = false;
        }
        
        fadeLedsBy(75);
        
        for (int i = 0; i < NUM_WAVES; i++) {
          // Bounce planes back and forth
          planes[i].dist = beatsin16(20, 0, 40000, 0, 13107 * i) - 20000;

          // Plane parameters
          planeDefaults();
          planeWidth = 7000;
          paletteIndex = millis()/20 + 51 * i + planes[i].dist / 300;
          
          renderPlane(&planes[i]);
          walkPlane(&planes[i], dirVectors[i], 40, 20, 1000);
        }
      }
      break;
      
     case 3: // Fireworks
      #define NUM_FIREWORKS 50
      static objPlane planes[NUM_FIREWORKS];
      if (newMode) {
        for (int i = 0; i < NUM_FIREWORKS; i++) {
          randomPlane(&planes[i], random(20000, 25000));
        }
        newMode = false;
      }

      fadeLedsBy(50);
      
      for (int i = 0; i < NUM_FIREWORKS; i++) {

        // Move plane inwards by set amount then create new plane
        planes[i].dist -= 100;
        if (planes[i].dist < 17000) {
          randomPlane(&planes[i], random(20000, 25000));
        }

        // Plane parameters
        planeDefaults();
        planeWidth = 1000;
        planeFade = 100;
        paletteIndex = millis() / 20 + 51 * i;

        renderPlane(&planes[i]);
      }
      break;

    case 4: // 3D noise
      if (newMode) {newMode = false;}
      FOR_ALL_LEDS {
        int index = inoise8(coords[i][0] / 100, coords[i][1] / 100, coords[i][2] / 100 + millis() / 20);
        leds[i] =  ColorFromPalette(gCurrentPalette, index + millis() / 20, 255);
      }
      break;

    case 5: // Sparkle
      if (newMode) {newMode = false;}
      
      fadeLedsBy(25);
      
      for (int i = 0; i < 40; i++) {
        int led = random(720);
        leds[led] = ColorFromPalette(gCurrentPalette, random(255), 255);
      }
      break;

    case 6: // Zoomies
      #define NUM_ZOOMS 12
      {
        static objPlane planes[NUM_ZOOMS];
        static int dirVectors[NUM_ZOOMS][3];
        if (newMode) {
          for (int i = 0; i < NUM_ZOOMS; i++) {
            randomPlane(&planes[i], 19000);
            randomUnitVector(dirVectors[i]);
          }
          newMode = false;
        }

        fadeLedsBy(25);

        for(int i = 0; i < NUM_ZOOMS; i++) {
          // Plane parameters
          planeDefaults();
          planeWidth = 1200;
          planeFade = 100;
          paletteIndex = millis() / 20 + 51 * i;

          renderPlane(&planes[i]);
          walkPlane(&planes[i], dirVectors[i], 300, 100, 1000);
        }
      }
      
      break;

    case 7: // Fireflies
      #define NUM_FLIES 40
      {
        static objPlane planes[NUM_FLIES];
        static int dirVectors[NUM_FLIES][3];
        if (newMode) {
          for (int i = 0; i < NUM_FLIES; i++) {
            randomPlane(&planes[i], 19000);
            randomUnitVector(dirVectors[i]);
          }
          newMode = false;
        }

        fadeLedsBy(25);

        for(int i = 0; i < NUM_FLIES; i++) {
          // Plane parameters
          planeDefaults();
          planeWidth = 700;
          paletteIndex = millis() / 20 + 40 * i;
          
          renderPlane(&planes[i]);
          walkPlane(&planes[i], dirVectors[i], 150, 1000, 1000);
        }
      }
      
      break;

    case 8: // Bloops
      #define NUM_BLOOPS 20
      {
        static objPlane planes[NUM_BLOOPS];
        static int dirVectors[NUM_BLOOPS][3];
        if (newMode) {
          for (int i = 0; i < NUM_BLOOPS; i++) {
            randomPlane(&planes[i], 17000);
            randomUnitVector(dirVectors[i]);
          }
          newMode = false;
        }

        fadeLedsBy(100);

        for(int i = 0; i < NUM_BLOOPS; i++) {
          // Plane parameters
          planeDefaults();
          planeWidth = 1800;
          paletteIndex = millis() / 20 + 51 * i;

          renderPlane(&planes[i]);
          planes[i].dist = 16500 + beatsin16(30 + i / 2, 0, 2300, 0, 3000 * i);
          walkPlane(&planes[i], dirVectors[i], 40, 20, 1000);
        }
      }
      break;

    case 9: // Orbits
      #define NUM_ORBITS 10
      {
        static objPlane planes[NUM_ORBITS];
        static int dirVectors[NUM_ORBITS][3];
        static int speeds[NUM_ORBITS];
        if (newMode) {
          for (int i = 0; i < NUM_ORBITS; i++) {
            randomPlane(&planes[i], 18850);
            randomUnitVector(dirVectors[i]);
            speeds[i] = random(250, 400);
          }
          newMode = false;
        }

        fadeLedsBy(30);

        for(int i = 0; i < NUM_ORBITS; i++) {
          // Plane parameters
          planeDefaults();
          planeWidth = 500;
          paletteIndex = millis() / 20 + 51 * i;
          
          renderPlane(&planes[i]);
          walkPlane(&planes[i], dirVectors[i], speeds[i], 20, 400);
        }

        static objPlane plane = {.v = {0, 0, 1000}};
        int bri = 100;
        int fadeStart = 19000;
        int fadeEnd = 3000;
        int dist = plane.dist;
        if (dist < 0) {dist *= -1;}

        if (dist < fadeEnd) {
          bri = 10;
        } else 
        if (dist < fadeStart) {
          bri = map(dist, fadeEnd, fadeStart, 0, 100);
        }

        // Plane parameters
        planeDefaults();
        planeWidth = 6000;
        planeFade = 75;
        paletteIndex = millis() / 50 + plane.dist / 300;
        planeBri = bri;
        
        renderPlane(&plane);
        plane.dist = -19000 + beatsin16(20, 0, 38000);
      }
      break;

    case 10: // Wobbly Planes
      #define NUM_WOBBLES 5
      {
        static objPlane planes[NUM_WOBBLES];
        // Vertical rotation vector
        int k[3] = {-707, 0, 707};
        if (newMode) {
          for (int i = 0; i < NUM_WOBBLES; i++) {
            int v[3] = {80, 27, 996};
            normalizeVector(v);
            rotateVector(v, k, 36000 / NUM_WOBBLES * i);
            planes[i].v[0] = v[0]; planes[i].v[1] = v[1]; planes[i].v[2] = v[2];
          }
          newMode = false;
        }

//        static objPlane spots[2];
//        if (newMode) {
//          for (int i = 0; i < 2; i++) {
//            spots[i] = k;
//          }
//          if (i == 0) {spots[3] = 17000;)
//          else if (i == 1) {spots[3] = -17000;)
//          newMode = false;
//        }
  
        fadeLedsBy(70);
  
        for (int i = 0; i < NUM_WOBBLES; i++) {
          // Plane parameters
          planeDefaults();
          planeMirror = true;
          planeWidth = 4000;
          paletteIndex = millis() / 50 + (255 / NUM_WOBBLES) * i;
          paletteScale = 50;

          for (int i = 0; i < NUM_WOBBLES; i++) {
            rotateVector(planes[i].v, k, 50);
          }
          renderPlane(&planes[i]);
        }

        for(int i = 0; i < 2; i++) {
          // Plane parameters
          planeDefaults();
          planeWidth = 1800;
          paletteIndex = millis() / 20 + 51 * i;

//          renderPlane(&spots[i]);
//          spots[i].dist = 16500 + beatsin16(30 + i / 2, 0, 2300, 0, 3000 * i);
        }
      }
      break;

    case 11: // Zaps
      #define NUM_ZAPS 5
      {
        static objPlane planes[NUM_ZAPS];
        if (newMode) {
          int v[3] = {0, 0, 1000};
          for (int i = 0; i < NUM_ZAPS; i++) {
            planes[i].v[0] = v[0]; planes[i].v[1] = v[1]; planes[i].v[2] = v[2];
            planes[i].dist = -19000 + beatsin16(20, 0, 38000, 0, 65536 / NUM_ZAPS * i);
          }
          newMode = false;
        }
  
        fadeLedsBy(70);
  
        for (int i = 0; i < NUM_ZAPS; i++) {
  
          // Plane parameters
          planeDefaults();
          planeMirror = true;
          planeWidth = 5000;
          paletteIndex = millis() / 50 + (255 / NUM_ZAPS) * i;
          paletteScale = 50;
  
          renderPlane(&planes[i]);
  
          planes[i].dist = -19000 + beatsin16(20, 0, 38000, 0, 65536 / NUM_ZAPS * i);
        }
      }
      break;

    default:
      mode = 0;
      break;
  }
}
