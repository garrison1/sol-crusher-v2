//
// --- Output Tools ---
//

void printVector(int v[3]) {
  Serial.print(v[0]); Serial.print(" ");
  Serial.print(v[1]); Serial.print(" ");
  Serial.print(v[2]); Serial.println(" ");
}

//
// --- LED Tools ---
//

void fadeLedsBy(int amount) {
  FOR_ALL_LEDS {
    leds[i].fadeToBlackBy(amount);
  }
}

//
// --- Plane Tools ---
//

void planeDefaults() {
  planeMirror = false;
  planeWidth = 4000;
  planeFade = 50;
  paletteIndex = 0;
  paletteScale = 100;
  planeBri = 100;
}

void walkPlane(objPlane *plane, int dVector[3], int rotSpeed, int randAmount, int maxZ) {
  int vector[3] = {plane->v[0], plane->v[1], plane->v[2]};
  int rVector[3];
  randomUnitVector(rVector);

  for (int i = 0; i < 3; i++) {
    dVector[i] += random(-randAmount, randAmount);
  }

  objPlane zeroPlane = {.v = {vector[0], vector[1], vector[2]}};
  int dist = distFromPlane(&zeroPlane, dVector);

  int mVector[3];
  for (int i = 0; i < 3; i++) {
    mVector[i] = vector[i] * dist / plane->dist;
    dVector[i] -= mVector[i];
  }
  
  normalizeVector(dVector);

  rotateVector(vector, dVector, rotSpeed);
  if (vector[2] > maxZ) {vector[2] = maxZ;}
  if (vector[2] < -maxZ) {vector[2] = -maxZ;}
  normalizeVector(vector);

  int vectorSize = sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
  for (int i = 0; i < 3; i++) {
    plane->v[i] = vector[i] * 1000 / vectorSize;
  }
}

void renderPlaneRaw(objPlane *plane, bool mirror, int wide, int fadePercent, CRGBPalette16 palette, 
                 int index, int paletteScale, int planeBri) {
  int wideHalf = wide / 2;
  FOR_ALL_LEDS {
    if (i % 4 != 1) { // Only outside LEDs
      // Calculate distance from plane
      int dist = distFromPlane(plane, coords[i]);
      if (mirror && dist < 0) {
        dist *= -1;
      }
      
      if (((-1 * wideHalf) < dist) && (dist < (wideHalf))) {
        CRGB color;
        int bri = planeBri;
        int distPercent = 100 - dist * (long)100 / wideHalf;
        if (distPercent < fadePercent) {
          // Calculate brightness
          bri = map(distPercent, 0, fadePercent, 0, planeBri);
        }
        int colorIndex = index + dist / paletteScale;
        color = ColorFromPalette(palette, colorIndex, bri);
  
        leds[i] += color;
      }
    }
  }
}

void renderPlane(objPlane *plane) {
  renderPlaneRaw(plane, planeMirror, planeWidth, planeFade, gCurrentPalette, paletteIndex, paletteScale, planeBri);
}

// Calculate distance from a point to a plane
// (Plane Vector (dot) Point - Plane Distance * 1000) / 1000
int distFromPlane(objPlane *plane, int point[3]) {
  long dot = plane->v[0] * (long)point[0] + plane->v[1] * (long)point[1] + plane->v[2] * (long)point[2];
  return (dot - (long)plane->dist * 1000) / 1000;
}

// Generate random plane
// Distance is mm x 100
void randomPlane(objPlane *plane, int dist) {
  int vector[3];
  randomUnitVector(vector);
  for (int i = 0; i < 3; i++) {
    plane->v[i] = vector[i];
  }
  plane->dist = dist;
}

//
// --- Vector Tools ---
//

// Rotate vector around another vector
// Uses Rodrigues' Rotation Formula
// Assumes k is a unit vector x 1000
void rotateVector(int v[3], int k[3], int t){
  float theta = 3.14 * t / 18000.;
  int dotResult = dot(v, k);
  int crossResult[3];
  
  cross(crossResult, k, v);
  for (int i = 0; i < 3; i++) {
  }
  
  float fk[3];
  for (int i = 0; i < 3; i++) {
    fk[i] = k[i] / 1000.;
  }
  float result[3];
  
  
  for (int i = 0; i < 3; i++) {
    result[i] = v[i] * cos(theta) + crossResult[i] * sin(theta) + fk[i] * dotResult * (1 - cos(theta));
  }

  normalizeVectorFloat(result);
  
  for (int i = 0; i < 3; i++) {
    v[i] = result[i];
  }
}

// Dot Product
// Assumes v2 is a unit vector x1000
int dot(int v1[3], int v2[3]) {
  float fv2[3];
  for (int i = 0; i < 3; i++) {
    fv2[i] = v2[i] / 1000.;
  }
  
  return v1[0] * fv2[0] + v1[1] * fv2[1] + v1[2] * fv2[2];
}


// Cross Product
// Assumes v is a unit vector x1000
void cross(int result[3], int u[3], int v[3]) {
  float fu[3];
  float fv[3];

  for (int i = 0; i < 3; i++) {
    fu[i] = (float)u[i];
    fv[i] = (float)v[i] / 1000;
  }
  
  result[0] = (int)(fu[1] * fv[2] - fu[2] * fv[1]);
  result[1] = (int)(fu[2] * fv[0] - fu[0] * fv[2]);
  result[2] = (int)(fu[0] * fv[1] - fu[1] * fv[0]);
}

// Generate random unit vector
// Vector is scaled by 1000 
void randomUnitVector(int v[3]) {
  int mag = 2000;
  while (mag > 1000) {
    // Generate random coordinates until the vector lies within the unit sphere
    v[0] = random(-1000, 1001);
    v[1] = random(-1000, 1001);
    v[2] = random(-1000, 1001);
    mag = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
  }

  // Normalize the vector
  normalizeVector(v);
}

// Normailzes vector to size 1000
void normalizeVector(int v[3]) {
  int mag = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
  v[0] = v[0] * 1000 / mag;
  v[1] = v[1] * 1000 / mag;
  v[2] = v[2] * 1000 / mag;
}

// Normailzes flaot vector to size 1000
void normalizeVectorFloat(float v[3]) {
  float mag = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
  v[0] = v[0] * 1000 / mag;
  v[1] = v[1] * 1000 / mag;
  v[2] = v[2] * 1000 / mag;
}
