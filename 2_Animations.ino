void transition() {
  // First half of transition
  if (transitionCounter > TRANSITION_LENGTH / 2) {
    // Calculate position of transition
    int pos = map(transitionCounter, TRANSITION_LENGTH, TRANSITION_LENGTH / 2, 19000 + TRANSITION_BLEND, -19000 - TRANSITION_BLEND);
    
    FOR_ALL_LEDS {
      // Generate transition gradient
      CRGB transitionColor = ColorFromPalette(gCurrentPalette, map(coords[i][2], -19000, 19000, 0, 255) + millis() / 20, 255);

      // Calculate distance from transition position
      int z = coords[i][2];
      int dist = z - pos;

      // Within TRANSITION_BLEND distance from position, blend in transition color from top
      if (dist > 0) {
        if (dist < TRANSITION_BLEND) {
          int blendAmount = map(dist, 0, TRANSITION_BLEND, 0, 255);
          leds[i] = blend(leds[i], transitionColor, blendAmount);
        } else {
          leds[i] = transitionColor;
        }
      }
    }
  } else 
  
  // Switch modes at halfway point
  if (transitionCounter == TRANSITION_LENGTH / 2) {
    int oldMode = mode;
    newMode = true;
    while (mode == oldMode) {
      mode = random(12);
//      mode++;
    }
    Serial.print("Animation: ");Serial.println(mode);
  } else 
  
  // Second half of transition
  if (transitionCounter < TRANSITION_LENGTH / 2) {
    // Calculate position of transition
    int pos = map(transitionCounter, TRANSITION_LENGTH / 2, 0, 19000 + TRANSITION_BLEND, -19000 - TRANSITION_BLEND);
    
    FOR_ALL_LEDS {
      // Generate transition gradient
      CRGB transitionColor = ColorFromPalette(gCurrentPalette, map(coords[i][2], -19000, 19000, 0, 255) + millis() / 20, 255);

      // Calculate distance from transition position
      int z = coords[i][2];
      int dist = z - pos;

      // Within TRANSITION_BLEND distance from position, blend out transition color from top
      if (dist < 0) {
        if (dist > -1 * TRANSITION_BLEND) {
          int blendAmount = map(dist, 0, -1 * TRANSITION_BLEND, 0, 255);
          leds[i] = blend(leds[i], transitionColor, blendAmount);
        } else {
          leds[i] = transitionColor;
        }
      }
    }
  }
  transitionCounter--;
}

// This function draws color waves with an ever-changing,
// widely-varying set of parameters, using a color palette.
void colorwaves( CRGB* ledarray, uint16_t numleds, CRGBPalette16& palette) 
{
  static uint16_t sPseudotime = 0;
  static uint16_t sLastMillis = 0;
  static uint16_t sHue16 = 0;
 
//  uint8_t sat8 = beatsin88( 87, 220, 255);
  uint8_t brightdepth = beatsin88( 341, 96, 255);
  uint16_t brightnessthetainc16 = beatsin88( 203, (25 * 256), (40 * 256));
  uint8_t msmultiplier = beatsin88(147, 23, 60);

  uint16_t hue16 = sHue16;//gHue * 256;
  uint16_t hueinc16 = beatsin88(113, 300, 1500);
  
  uint16_t ms = millis();
  uint16_t deltams = ms - sLastMillis ;
  sLastMillis  = ms;
  sPseudotime += deltams * msmultiplier;
  sHue16 += deltams * beatsin88( 400, 5,9);
  uint16_t brightnesstheta16 = sPseudotime;
  
  for ( uint16_t i = 0 ; i < numleds; i++) {
    hue16 += hueinc16;
    uint8_t hue8 = hue16 / 256;
    uint16_t h16_128 = hue16 >> 7;
    if ( h16_128 & 0x100) {
      hue8 = 255 - (h16_128 >> 1);
    } else {
      hue8 = h16_128 >> 1;
    }

    brightnesstheta16  += brightnessthetainc16;
    uint16_t b16 = sin16( brightnesstheta16  ) + 32768;

    uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
    uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
    bri8 += (255 - brightdepth);
    
    uint8_t index = hue8;
    //index = triwave8( index);
    index = scale8( index, 240);

    CRGB newcolor = ColorFromPalette( palette, index*2, bri8);

    uint16_t pixelnumber = i;
    pixelnumber = (numleds-1) - pixelnumber;
    
    nblend(ledarray[pixelnumber], newcolor, 128);
  }
}

// Alternate rendering function just scrolls the current palette 
// across the defined LED strip.
void palettetest( CRGB* ledarray, uint16_t numleds, const CRGBPalette16& gCurrentPalette)
{
  static uint8_t startindex = 0;
  startindex--;
  fill_palette( ledarray, numleds, startindex, (256 / NUM_LEDS) + 1, gCurrentPalette, 255, LINEARBLEND);
}
