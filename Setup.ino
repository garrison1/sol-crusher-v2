void setupN() {
  Serial.println("----- SETUP PANELS -----");
  
  for (int panelNum = 0; panelNum < 32; panelNum++) {

    bool isPanelSetup = false;

    Serial.print("Setup Panel "); Serial.println(panelNum);

    // Get first unassigned channel
    int channelNum = -1;
    channelNum = cycleChannel(channelNum, 1); 

    // Temporarilly assign channel
    assignPanel(panelNum, channelNum, 0);
    setPanelColor(panelNum, CRGB::Orange);
    render(leds);

    // Setup Panel
    while (!isPanelSetup) {
      
      // Get Input
      int keyPress = input();

      // Show Orientation LEDs on a neighbor
      int neighborPanel = panelNeighbors[panelNum];                   // Get neighboring panel from list
      if (panelNum != 0) {
        getClosestLeds(closestLeds, neighborPanel, panelNum);         // Calculate closest LEDs to each other
        int startLedNeighbor = getStartLed(neighborPanel);            // Get first LED in neighboring panel
        leds[startLedNeighbor + closestLeds[0]] = CRGB::White;        // Set LEDS white
        leds[startLedNeighbor + closestLeds[2]] = CRGB::White;
        render(leds);
      }

      // Accept channel assignment
      if (keyPress == 1) {

        // Asign panel to channel
        isPanelSetup = true;
        setupChannels[channelNum] = panelNum;
        Serial.print("Channel "); Serial.print(channelNum); Serial.print(" assigned to Panel "); Serial.println(panelNum);

        // Show Orientation LEDs
        setPanelColor(panelNum, CRGB(0, 50, 50));
        if (panelNum == 0) {              // Orientation LEDs are around x = 0 for Panel 0
          leds[2] = CRGB::White;
          leds[19] = CRGB::White;
          render(leds);
        } else {                          // For all other channels it's the LEDs that should touch the neighbor
          int startLed = getStartLed(panelNum);
          leds[startLed + closestLeds[1]] = CRGB::White;
          leds[startLed + closestLeds[3]] = CRGB::White;
          render(leds);
        }

        // Variables for orientation setup
        bool isOriented;
        isOriented = false;
        int orient;
        orient = 0;

        Serial.print("Set orientation for Panel "); Serial.println(panelNum);

        while (!isOriented) {
          
          // Get Input
          keyPress = input();

          // Accept orientation
          if (keyPress == 1) {
            setPanelColor(panelNum, CRGB(0, 50, 0));
            setPanelColor(neighborPanel, CRGB(0, 50, 0));
            isOriented = true;
            Serial.print("Orientation is "); Serial.println(orient); Serial.println(" ");
          } else 

          // Next Orientation
          if (keyPress == 2) {
            orient++;
            if (orient > (panelOrder[panelNum] / 4 - 1)) {orient = 0;}
            assignPanel(panelNum, channelNum, orient * 4);
            render(leds);
//            Serial.println(orient);
          } else

          // Previous Orientation
          if (keyPress == 3) {
            orient--;
            if (orient < 0) {orient = panelOrder[panelNum] / 4 - 1;}
            assignPanel(panelNum, channelNum, orient * 4);
            render(leds);
//            Serial.println(orient);
          }

          render(leds);
          FastLED.delay(200);
        }
        
      } else if (keyPress == 2 || keyPress == 3) {

        // Clear Panel
        setPanelColor(panelNum, CRGB::Black);
        render(leds);
        clearPanel(panelNum);
          
        // Next Channel
        if (keyPress == 2) {
          channelNum = cycleChannel(channelNum, 1);
        } else
  
        // Previous Channel
        if (keyPress == 3) {
          channelNum = cycleChannel(channelNum, -1);
        }

        // Temporarilly assign channel
        assignPanel(panelNum, channelNum, 0);
        setPanelColor(panelNum, CRGB::Orange);
        render(leds);
        FastLED.delay(200);
      } 
    }
  }
  isSetup = true;
  Serial.println("-- Setup Complete --");
  Serial.println("LED to raw LED conversion list follows:");
  Serial.print("{");
  for (int i = 0; i < NUM_LEDS - 1; i++) {
    Serial.print(assignments[i]); Serial.print(", ");
  }
  Serial.print(assignments[NUM_LEDS - 1]); Serial.println("}");
  
  FastLED.delay(2000);
}

void getClosestLeds(int *ledList, int panel1, int panel2) {
  for (int i = 0; i < 4; i++) {
    ledList[i] = 0;
  }
  int numLeds1 = panelOrder[panel1];
  int startLed1 = getStartLed(panel1);
  int numLeds2 = panelOrder[panel2];
  int startLed2 = getStartLed(panel2);

  int distances[24][24];

  for (int i = 0; i < 24; i++) {
    for (int j = 0; j < 24; j++) {
      distances[i][j] = 32767;
    }
  }
  
  for (int i = 0; i < numLeds1; i++) {
    for (int j = 0; j < numLeds2; j++) {
      int dx = coords[startLed2 + j][0] - coords[startLed1 + i][0];
      int dy = coords[startLed2 + j][1] - coords[startLed1 + i][1];
      int dz = coords[startLed2 + j][2] - coords[startLed1 + i][2];
      distances[i][j] = sqrt(dx*dx + dy*dy + dz*dz);
    }
  }

  int pair1Dist = 32767;
  int pair2Dist = 32767;
  
  for (int i = 0; i < 24; i++) {
    for (int j = 0; j < 24; j++) {
      if (distances[i][j] <= pair1Dist) {     // If distance is less than the pair 1 (2nd closest)
        if (distances[i][j] <= pair2Dist) {     // And pair 2 (closest)
          pair1Dist = pair2Dist;                  // Move pair 2 to pair 1
          ledList[0] = ledList[2];
          ledList[1] = ledList[3];
          
          pair2Dist = distances[i][j];            // Set new pair in pair 2 (closest)
          ledList[2] = i;
          ledList[3] = j;
        } else {
          pair1Dist = distances[i][j];            // Else set new pair in pair 1 (2nd closest)
          ledList[0] = i;
          ledList[1] = j;
        }
      }
    }
  }
}

int getStartLed(int panelNum) {
  // Calculate first LED for the panel
  int startLed = 0;
  for (int j = 0; j < panelNum; j++) {  // Add up leds in previous panels
    startLed += panelOrder[j];
  }
  return startLed;
}

void assignPanel(int panelNum, int channelNum, int offst) {

  // Get number of LEDs in panel
  int numLeds = panelOrder[panelNum];

  int startLed = getStartLed(panelNum);

  // Assign LEDs after offset
  for (int j = 0; j < numLeds; j++) {
    int channelLed = j + offst;
    if (channelLed >= numLeds) {channelLed -= numLeds;}
    assignments[startLed + j] = channelNum * 24 + channelLed;
  }
}

void clearPanel(int panelNum) {

  // Calculate first LED for the panel
  int startLed = 0;
  for (int j = 0; j < panelNum; j++) {
    startLed += panelOrder[j];
  }
  
  for (int j = 0; j < panelOrder[panelNum]; j++) {
    assignments[startLed + j] = -1;
  }
}

void setPanelColor(int panelNum, CRGB color) {

  // Calculate first LED for the panel
  int startLed = 0;
  for (int j = 0; j < panelNum; j++) {
    startLed += panelOrder[j];
  }
  
  for (int i = startLed; i < startLed + panelOrder[panelNum]; i++) {
    leds[i] = color;
  }
}

int cycleChannel(int channelNum, int inc) {
  
  // Go to next or previous panel
  channelNum += inc;
  // Check for roll-over
  if (channelNum == 32) {channelNum = 0;}
  else if (channelNum == -1) {channelNum = 31;}

  // If new channel isn't free, loop for next one
  while (setupChannels[channelNum] != -1) {
//    Serial.print("Channel "); Serial.print(channelNum); Serial.print(": "); Serial.println(setupChannels[channelNum]);
    channelNum += inc; 
    if (channelNum == 32) {channelNum = 0;}
    else if (channelNum == -1) {channelNum = 31;}
  }
  Serial.print("Channel "); Serial.print(channelNum); Serial.println(" is free");
  return channelNum;
}

int input() {
  char rx_byte;
  if (Serial.available() > 0) {    // Is a character available?
    rx_byte = Serial.read();       // Get the character
    
    if (rx_byte == 'y') {
      // Accept Setting
//      Serial.println("Input: Accept");
      return 1;
    } else
    if (rx_byte == 'n') {
      // Next Value
//      Serial.println("Input: Next");
      return 2;
    } else
    if (rx_byte == 'p') {
      // Previous Value
//      Serial.println("Input: Previous");
      return 3;
    } else {
      return 0;
    }
  }
  return 0;
}
