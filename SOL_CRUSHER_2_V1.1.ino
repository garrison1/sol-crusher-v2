#include <FastLED.h>
#include <EEPROM.h>
#include <Average.h>
#include "Coordinates.h"

#define DATA1_PIN    21
#define DATA2_PIN    22
#define DATA3_PIN    23
#define DATA4_PIN    33
#define DATA5_PIN    19
#define DATA6_PIN    20
#define DATA7_PIN    32
#define DATA8_PIN    18
#define DATA9_PIN    17
#define DATA10_PIN   31
#define DATA11_PIN   16
#define DATA12_PIN   30
#define DATA13_PIN   15
#define DATA14_PIN   29
#define DATA15_PIN   14
#define DATA16_PIN   13
#define DATA17_PIN   4
#define DATA18_PIN   3
#define DATA19_PIN   2
#define DATA20_PIN   24
#define DATA21_PIN   6
#define DATA22_PIN   5
#define DATA23_PIN   25
#define DATA24_PIN   7
#define DATA25_PIN   8
#define DATA26_PIN   26
#define DATA27_PIN   9
#define DATA28_PIN   27
#define DATA29_PIN   10
#define DATA30_PIN   28
#define DATA31_PIN   11
#define DATA32_PIN   12
#define CLOCK_PIN   1

// Setup order of hexagons and pentagons from top to bottom
int panelOrder[32] = {20, 24, 24, 24, 24, 24, 20, 20, 20, 20, 20, 24, 24, 24, 24, 24,
24, 24, 24, 24, 24, 20, 20, 20, 20, 20, 24, 24, 24, 24, 24, 20};

#define NUM_LEDS_RAW  768
#define NUM_LEDS      720
#define FOR_ALL_LEDS  for (int i = 0; i < NUM_LEDS; i++)
#define BRIGHTNESS    100
#define LED_TYPE      APA102
#define COLOR_ORDER   BGR
CRGB rawLeds[NUM_LEDS_RAW];

bool setupPanels[32];         // Whether each panel has been assigned
int setupChannels[32];        // Holds assignments of panels to channels
bool isSetup = true;          // Go into setup mode or not
int closestLeds[4];           // {2nd closest LED1, 2nd closest LED2, 1st closest LED1, 1st closest LED2}
#define NUM_MODES
int mode = -1;
bool newMode = true;

#define TRANSITION_LENGTH 100
#define TRANSITION_BLEND 5000
int transitionCounter = TRANSITION_LENGTH;
Average<int> fpsAve(50);
Average<int> renderAve(50);

bool planeMirror = false;
int planeWidth = 4000;
int planeFade = 50;
int paletteIndex = 0;
int paletteScale = 100;
int planeBri = 100;

int panelNeighbors[32] = {0,  // List of neighboring panels for alingment
0, 0, 0, 0, 0, 
1, 2, 3, 4, 5, 
1, 2, 3, 4, 5, 
6, 7, 8, 9, 10,
11, 12, 13, 14, 15,
16, 17, 18, 19, 20,
30};

typedef struct{
   int v[3];
   int dist;
} objPlane;

CRGB leds[NUM_LEDS];          // Array of actual LEDs

void setup() {  
  randomSeed(analogRead(A1));
  
  FastLED.addLeds<LED_TYPE, DATA1_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 0, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA2_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 24, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA3_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 48, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA4_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 72, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA5_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 96, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA6_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 120, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA7_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 144, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA8_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 168, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA9_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 192, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA10_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 216, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA11_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 240, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA12_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 264, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA13_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 288, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA14_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 312, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA15_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 336, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA16_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 360, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA17_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 384, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA18_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 408, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA19_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 432, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA20_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 456, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA21_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 480, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA22_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 504, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA23_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 528, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA24_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 552, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA25_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 576, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA26_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 600, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA27_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 624, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA28_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 648, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA29_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 672, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA30_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 696, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA31_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 720, 24).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<LED_TYPE, DATA32_PIN, CLOCK_PIN, COLOR_ORDER>(rawLeds, 744, 24).setCorrection( TypicalLEDStrip );
  
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.setMaxPowerInVoltsAndMilliamps(5,1000);
  FastLED.show();

  Serial.begin(9600);
  delay(500);
  Serial.println("-- SOL CRUSHER ACTIVATED --");

  // Set all channels to unassigned 
  for (int i = 0; i < 32; i++) {
    setupChannels[i] = -1;
  }  
}

// Forward declarations of an array of cpt-city gradient palettes, and 
// a count of how many there are.  The actual color palette definitions
// are at the bottom of this file.
extern const TProgmemRGBGradientPalettePtr gGradientPalettes[];
extern const uint8_t gGradientPaletteCount;

// Current palette number from the 'playlist' of color palettes
uint8_t gCurrentPaletteNumber = 0;

CRGBPalette16 gCurrentPalette( gGradientPalettes[0] );
CRGBPalette16 gTargetPalette( gGradientPalettes[0] );


void loop() {
  if (!isSetup) {
    // Set all LED ids to -1 (unassigned)
    FOR_ALL_LEDS {
      assignments[i] = -1;
      }
    setupN();
  }
  
  EVERY_N_SECONDS(15) {
    gCurrentPaletteNumber = addmod8( gCurrentPaletteNumber, random(20), gGradientPaletteCount);
    gTargetPalette = gGradientPalettes[ gCurrentPaletteNumber ];
  }

  EVERY_N_MILLISECONDS(50) {
    nblendPaletteTowardPalette( gCurrentPalette, gTargetPalette, 16);
  }  

  EVERY_N_SECONDS(30)  {
    // Start mode transition
    transitionCounter = TRANSITION_LENGTH;
    Serial.print("FPS Mean:      "); Serial.println(1000000/fpsAve.mean());
    Serial.print("Micros Mean:   "); Serial.println(fpsAve.mean());
    Serial.print("LED Data Mean: "); Serial.println(renderAve.mean());
  }

//  EVERY_N_MILLISECONDS(20) {
    static int t;
//    Serial.println(millis() - t);
    fpsAve.push(micros() - t);
    t = micros();


    animations(mode);

    if (transitionCounter) {
      transition();
    }
//  }
  int rt = micros();
  render(leds);
  renderAve.push(micros() - rt);
}

void setRawLeds() {
  FOR_ALL_LEDS {
    rawLeds[assignments[i]] = leds[i];
  }
}

void render(CRGB arry[NUM_LEDS]) {
  setRawLeds();
  FastLED.show();
}
